# Modeling with UML Sequence Diagrams

Code is a difficult medium to use to communicate designs to humans. It is useful to have a diagram language that abstracts away the details and allow a human to grasp the essential features of a design.

## Content Learning Objectives

After completing this activity, students should be able to:

- Identify parts of UML sequence diagrams.
- Connect UML class sequence elements with implementation details in Java code.
- Draw UML sequence diagrams in Markdown with PlantUML

## Process Skill Goals

During the activity, students should make progress toward:

- Interpreting diagrams and differing representations of the same program. (Information Processing)
- Determining the placement of attributes and operations based on UML sequence diagram notations. (Critical Thinking)
- Creating UML sequence diagrams in Markdown with PlantUML (Oral and Written Communication)

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: UML Sequence Diagrams

The model shows two different representations of the same program: a UML Sequence Diagram on the top, and Java code below.

```plantuml
hide footbox

actor User

create participant student
User -> student: new
activate student
student -> student: nextIdNumber++
create participant schedule
student -> schedule: new
activate schedule
student <-- schedule: schedule
deactivate schedule
User <-- student: student
deactivate student

create participant course
User -> course: new
activate course
User <-- course: course
deactivate course

User -> student: addCourse(course)
activate student
student -> schedule: add(course)
activate schedule
student <-- schedule
deactivate schedule
User <-- student
deactivate student

User -> student: getPrintedSchedule
activate student

create participant string
student -> string: new

group loop [for each course in schedule]
    student -> schedule: get next course
    activate schedule
    student <-- schedule: course
    deactivate schedule
    student -> string: concatenate course
    activate string
    student <-- string: string
    deactivate string
end

User <-- student: string
deactivate student
User -> string: print string
destroy string
```

```java
public class User {
    public static void main(String args) {
        Student student = new Student("Jane", "Doe");
        Course course = new Course("CS", 343, 1, 3);
        student.addCourse(course);
        System.out.println(student.getPrintableSchedule);
        // continues...
    }
}
```

```java
public class Student {
    private static int nextIdNumber;
    private String firstName;
    private String lastName;
    private String id;
    private Collection<Course> schedule
        = new java.util.ArrayList<Course>();

    public void addCourse(Course course) {
        schedule.add(course);
    }

    public void removeCourse(Course course) {
        schedule.remove(course);
    }

    public String getPrintableSchedule() {
        String printableSchedule = new String();
        for course : schedule {
            printableSchedule += course + "\n";
        }
        return printableSchedule;
    }

    public Student(String firstName, String lastName) {
        this.id = nextIdNumber++;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
```

### Questions (15 min)

1. For each of the following in the diagram, does it represent a class or an object? Why? Base your answer on the code (not just on the formatting of the names).
    - `User`
    - `student`
    - `schedule`
    - `course`
2. What type of PlantUML "thing" is each of the following in the PlantUML code? Why aren't they all the same type?
    - `User`
    - `student`
    - `schedule`
    - `course`
3. Why don't `User`, `student`, `schedule`, `course`, and `string` all appear at the very top of the diagram? How is that represented in the PlantUML code?
4. What does the vertical "axis" of the diagram represent?
5. What do the solid-line, horizontal arrows represent? What does the label text on those arrows represent? How is this represented in the PlantUML code?
6. What do the dashed-line, horizontal arrows represent? What does the label test on those arrows represent? How is this represented in the PlantUML code?
7. What do the narrow, vertical boxes represent? How is this represented in the PlantUML code?
8. Why are there multiple narrow, vertical boxes for `student` and `schedule`?
9. Why do `course` and `string` only have one narrow, vertical box each?
10. Is there anything in the code for `student` that determines how many narrow, vertical boxes for `student` has? What *does* determine this?
11. Compare the loop box in the diagram to the PlantUML code that creates it. Make notes on how it works.
12. The &#10060; on the *lifeline* for `string` indicates the destruction of the object (or in Java being scheduled for garbage collection). How is this represented in the PlantUML code?
13. Why is `string` destroyed? Why aren't `student`, `schedule`, and `course` marked as being destroyed?

## Model 2: Modifying UML Sequence Diagrams

We are going to extend the program so that the student has four courses on their schedule. The following lines have been added to `main` right before the `System.out.println`:

```java
    Course course2 = new Course("CS", 348, 2, 3);
    student.addCourse(course2);
    Course course3 = new Course("CS", 443, 1, 3);
    student.addCourse(course3);
    Course course4 = new Course("CS", 448, 1, 3);
    student.addCourse(course3);
```

You can simplify the PlantUML code in three different ways. See the [PlantUML page for Sequence Diagrams](https://plantuml.com/sequence-diagram), in the following sections:

- Lifeline Activation and Destruction (the part about autoactivation)
- Return
- Shortcut syntax for activation, deactivation, creation

<!-- markdownlint-disable MD028 -->
> Paste the PlantUML code for the diagram from Model 1 here and modify it according to the instructions in the questions below.
<!-- markdownlint-disable MD028 -->

<!-- markdownlint-disable MD024 -->
### Questions (15 min)
<!-- markdownlint-enable MD024 -->

1. Add the PlantUML code for adding course2. Use `return` rather than the left-pointing dashed arrow, and the `deactivate`.
2. Add the PlantUML code for adding course3. Use the shortcut syntax rather `activate` and `deactivate`.
3. Add the PlantUML code for adding course4. Use the autoactivation syntax rather `activate` and `deactivate`. I suggest turning autoactivation on at the beginning of this section, and off again at the end of this section.
4. Discuss the 4 different ways to do this. What are the advantages and disadvantages. Do you have a preference. Make notes here.
5. What does `hide footbox` do? Remove it to find out. Make sure you add it back before moving on.

## Model 3: Types of UML Diagrams

UML has two major classifications of diagrams - Structure and Behavior.

<!-- markdownlint-disable MD024 -->
### Questions (5 min)
<!-- markdownlint-enable MD024 -->

1. Classify `UML Class Diagrams` and `UML Sequence Diagrams` as either Structure or Behavior and explain your reasoning.
2. Take a look at the [UML 2.5 Diagrams Overview](https://www.uml-diagrams.org/uml-25-diagrams.html) to see other diagrams that fit in each of these categories.
3. Take a look at [PlantUML at a Glance](https://plantuml.com/) to see what type of diagrams (both UML and other) can be represented with PlantUML.

Copyright © 2023 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
